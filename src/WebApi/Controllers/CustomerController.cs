using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Db.Repositories;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {

        private readonly IRepository<Customer> _customerRepository;
        public CustomerController(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [HttpGet("{id:long}")]
        public async Task<ActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer != null)
            {
                return Ok(customer);
            }
            return NotFound();

        }

        [HttpPost("")]   
        public async Task<ActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {

            var existedCustomer = _customerRepository.GetByIdAsync(customer.Id).Result;
            
            if (existedCustomer is not null)
            {
                return Conflict();
            }
            
            await _customerRepository.AddAsync(customer);
            return Ok(customer.Id);
        }
    }
}