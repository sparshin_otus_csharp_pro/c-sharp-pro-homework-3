﻿using System.Threading.Tasks;
using System.Linq;
using WebApi.Db.Contexts;
using System.Collections.Generic;
using WebApi.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DbInitializers
{
    public class DbInitializer
            : IDbInitializer
    {
        private readonly SQLiteDbContext _sqliteDbContext;

        public DbInitializer(SQLiteDbContext dbContext)
        {
            _sqliteDbContext = dbContext;
        }

        public void InitializeDb()
        {
            //создаем базу, если надо
            _sqliteDbContext.Database.EnsureCreated();

            //пару-тройку изначальных кастомеров
            if (!_sqliteDbContext.Customers.Any())
            {
                _sqliteDbContext.Customers.AddRange(GenerateInitialCustomers());
            }
            _sqliteDbContext.SaveChanges();
        }

        private List<Customer> GenerateInitialCustomers() =>
            new List<Customer>()
            {
                new Customer()
                {
                    Id = 1,
                    Firstname = "Иван",
                    Lastname = "Иванов"
                },
                new Customer()
                {
                    Id = 2,
                    Firstname = "Петр",
                    Lastname = "Петров"
                },
                new Customer()
                {
                    Id = 3,
                    Firstname = "Семен",
                    Lastname = "Семенов"
                },
            };
    }

}
