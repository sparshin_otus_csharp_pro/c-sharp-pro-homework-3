﻿using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Db.Contexts
{
    public class SQLiteDbContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public SQLiteDbContext(DbContextOptions<SQLiteDbContext> options) : base(options)
        {

        }

    }
}
