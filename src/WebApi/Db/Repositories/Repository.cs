﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Db.Contexts;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Db.Repositories
{
    public class Repository<T>: IRepository<T> where T : BaseEntity
    {
        private readonly SQLiteDbContext _sqliteDbContext;

        public Repository(SQLiteDbContext sqliteDbContext)
        {
            _sqliteDbContext = sqliteDbContext;
        }

        public async Task AddAsync(T entity)
        {
            await _sqliteDbContext.Set<T>().AddAsync(entity);
            await _sqliteDbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _sqliteDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(long id)
        {
            return await _sqliteDbContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
