﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Db.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(long id);
        Task AddAsync(T entity);
    }
}
