﻿namespace WebApi.Db
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}
