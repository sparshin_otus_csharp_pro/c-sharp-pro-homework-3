﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {

        private static readonly HttpClient client = new HttpClient();
        private static Random random = new Random((int)DateTime.Now.Ticks);

        //можно в конфиг засунуть
        private const string serviceURL = "https://localhost:5001/customers/";

        static async Task Main(string[] args)
        {
            PrintCommandList();

            var exit = false;
            do
            {
                Console.WriteLine("Enter the command");

                var command = Console.ReadLine();
                switch (command.ToLower())
                {
                    case "getcustomer":
                        await ExecutetGetCustomerCommandAsync();
                        break;
                    case "addcustomer":
                        await ExecuteAddCustomerCommandAsync();
                        break;
                    case "exit":
                        exit = true;
                        break;
                    default:
                        PrintCommandList();
                        break;
                }

            }
            while (!exit);

            Console.WriteLine("Good bye!");
            Thread.Sleep(3000);
        }

        private static async Task ExecutetGetCustomerCommandAsync()
        {
            try
            {
                Console.WriteLine("Enter Customer ID (long integer)");
                var customerIdStr = Console.ReadLine();

                if (long.TryParse(customerIdStr, out long customerId))
                {
                    await GetCustomerById(customerId);
                }
                else
                {
                    Console.WriteLine("ID is not long integer");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private static async Task ExecuteAddCustomerCommandAsync()
        {

            var customerRequest = RandomCustomer();

            HttpContent content = new StringContent(JsonConvert.SerializeObject(customerRequest), Encoding.UTF8, "application/json");
            var responce = await client.PostAsync("https://localhost:5001/customers", content);
            if (responce != null && responce.IsSuccessStatusCode)
            {
                var result = await responce.Content.ReadAsStringAsync();
                if (int.TryParse(result, out int customerId))
                {
                    await GetCustomerById(customerId);
                }
            }
            

            
        }

        private static async Task GetCustomerById(long customerId)
        {
            var httpResponse = await client.GetAsync($"{serviceURL}{customerId}");

            if (httpResponse.IsSuccessStatusCode)
            {
                var json = httpResponse.Content.ReadAsStringAsync().Result;
                var customer = JsonConvert.DeserializeObject<Customer>(json);
                Console.WriteLine($"CustomerId: {customer.Id}; First name: {customer.Firstname}; Last name: {customer.Lastname}");
            }
            else
            {
                Console.WriteLine(httpResponse.StatusCode);
            }
        }

        private static void PrintCommandList()
        {
            Console.WriteLine("Сommand list:");
            Console.WriteLine("GetCustomer - write customer data by Id");
            Console.WriteLine("AddCustomer - add random customer");
            Console.WriteLine("Exit - exit the program");
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest(RandomString(8), RandomString(8));
        }

        private static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}